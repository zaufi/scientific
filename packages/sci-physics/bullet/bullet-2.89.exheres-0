# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ user=bulletphysics pn=bullet3 tag=${PV} ] cmake

SUMMARY="A professional free 3D game multiphysics library"
HOMEPAGE+=" http://www.bulletphysics.com/"

LICENCES="ZLIB"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    openmp [[ description = [ Support for multi-threading with OpenMP ] ]]
"

DEPENDENCIES="
    build+run:
        x11-dri/freeglut
        x11-dri/mesa
        openmp? ( sys-libs/libgomp:= )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_BULLET3:BOOL=TRUE
    -DBUILD_EXTRAS:BOOL=TRUE
    -DBUILD_PYBULLET:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DINSTALL_LIBS:BOOL=TRUE
    -DUSE_SOFT_BODY_MULTI_BODY_DYNAMICS_WORLD:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'openmp BULLET2_MULTITHREADING'
    'openmp BULLET2_USE_OPEN_MP_MULTITHREADING'
)

# fails to build with _DEMOS disabled
# https://github.com/bulletphysics/bullet3/issues/1246
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_BULLET2_DEMOS:BOOL=TRUE -DBUILD_BULLET2_DEMOS:BOOL=FALSE'
    '-DBUILD_CPU_DEMOS:BOOL=TRUE -DBUILD_CPU_DEMOS:BOOL=FALSE'
    '-DBUILD_OPENGL3_DEMOS:BOOL=TRUE -DBUILD_OPENGL3_DEMOS:BOOL=FALSE'
    '-DBUILD_UNIT_TESTS:BOOL=TRUE -DBUILD_UNIT_TESTS:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    edo rmdir "${IMAGE}"/usr/$(exhost --target)/include/bullet/BulletInverseDynamics/details
}

